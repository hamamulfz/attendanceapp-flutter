# sms_retriever

A new Flutter plugin to retrieve the SMS on Android using SMS Retrieval API

## Aditional Note

The Project is located in ```flutter-app``` folder. This app cannot recognize dependency installed on ```/home/hfauzy/flutter/.pub-cache/hosted/pub.dartlang.org/sms_retriever-0.0.2```.
The temporary solution is locate the project inside dependency and set pubspec.yaml depencency using 
```
  sms_retriever:
    path: ../
```

open the flutter-app to open project. Do not open via sms-retriever folder.


## Getting Started

To retrieve a app signature. It requires by the SMS
```dart
String appSignature = await SmsRetriever.getAppSignature();
```
To start listening for an incoming SMS
```dart
String smsCode = await SmsRetriever.startListening();
```
Stop listening after getting the SMS
```dart
SmsRetriever.stopListening();
```

Generate appSignature for keystore file
````dart in html
keytool -storepass storepass -alias alias -exportcert -keystore file | xxd -p | tr -d "[:space:]" | xxd -r -p | base64 | cut -c1-11

````

Example SMS

[#] Your example code is:
123456
appSignature